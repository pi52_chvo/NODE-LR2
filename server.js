var express = require('express');

var app = express();

var server = require('http').Server(app);

var io = require('socket.io');

io = io.listen(3030);

io.sockets.on("connection", (socket) => {
	socket.emit('userConnect', {message: 'Вы успешно подключились к чату!'});
	socket.on('newUser', (user) => {
		socket.broadcast.emit('newUser', user);
	});
	socket.on('disconnect', () => {
		
	});
	socket.on('sendMessage', (data) => {
		data.date = new Date();
		socket.broadcast.emit('getMessage', data);
	})
});

app.use(express.static(__dirname));

app.get('/', (req, res) => {
	res.sendfile(__dirname + '/index.html');
});

app.listen(3000, () => {
	console.log('server is started');
});
