$(document).ready(() => {
	var app = new Vue({
		el: '#App',
		data: {
			current_user: {
				isSignIn: false,
				name: null
			},
			users_list: [],
			messages: [],
			input_message: {
				author: null,
				text: null
			},
			socket: null
		},
		computed: {
			messagesCount() {
				return this.messages.length;
			}
		},
		methods: {
			login: function() {
				if(this.current_user.name.length > 0) {
					this.current_user.isSignIn = true;
					this.socket = io.connect('http://localhost:3030');
					this.socket.emit('newUser', {name: this.current_user.name});
					this.socket.on('userConnect', (data) => {

					});
					this.socket.on('getMessage', (data) => {
						this.messages.push(data);
					})

					this.socket.on('newUser', (data) => {
						this.messages.push({
							author: data.name,
							text: 'was connected',
							date: new Date()
						});
					})
				}
			},
			sendMessage: function() {
				this.input_message.author = this.current_user.name;
				console.log(this.input_message);
				this.socket.emit('sendMessage', this.input_message);
				this.input_message.date = new Date();
				this.messages.push(this.input_message);
			}
		}
	});
});
